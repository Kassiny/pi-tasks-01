/*Имеется строка, содержащая символьное представление числа,
 *  например "12345". Преобразовать в строку,
 *  где группы разрядов отделены пробелами,
 *  то есть "12 345"*/
#define N 256
#include <stdio.h>
#include <math.h>

int main(void)
{
    char str[N];
    char ans[N];
    int i;
    fgets(str,N,stdin);
    str[strlen(str)-1]=0;
    int len=strlen(str);

    for(i=0;i<len;i++)
    {
        if (!(str[i]>='0' && str[i]<='9'))
        {
            printf("ERROR");
            return 1;
        }
    }
    i=0;
    int start=len%3;//сколько разрядов надо отделить в начале
    int shift=0;//сдвиг из-за пробелов
    if (start!=0)
    {
        for(i=0;i<start;i++)
        {
            ans[i]=str[i];
        }
        ans[i]=' ';
        i++;
        shift=1;
    }
    int spaces=len/3;//количество пробелов, которые надо вставить
    int j;
    while (spaces>0)
    {
        for(j=0;j<3;j++)
        {
            ans[i+j]=str[i+j-shift];
        }
        ans[i+j]=' ';
        shift++;
        i+=(j+1);
        spaces--;

    }

    ans[i-1]='\0';
    printf("%s",ans);
    return 0;
}

