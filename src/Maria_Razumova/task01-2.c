/*Написать программу,\
принимающую величину роста в американской системе (футы'дюймы)\
и выводящую в европейской (см).\
В одном футе 12 дюймов, в 1 дюйме 2.54 см.\
При неправильном вводе программа должна выводить сообщение об ошибке.*/

#include <stdio.h>


int main()
{
    printf("Enter your height in American system: ");
    int feet=-1,inches=-1;
    float sm;
    //char height[256]
    if (!scanf("%d'%d",&feet,&inches))
    {
         printf("ERROR");
         return 1;
    }
    if (feet<0 || inches<0)
    {
        printf("ERROR");
        return 2;
    }
    inches=inches+feet*12;
    sm=inches*2.54f;
    printf("Height %0.2f sm",sm);
    return 0;
}

