//Написать программу, которая\
запрашивает сумму в виде "15.20"\
и выводит ее в измененном виде: "15 руб. 20 коп."
#define N 255
#include <stdio.h>
#include <string.h>


int main()
{
    char summ[N];
    printf("Enter your summ: ");
    fgets(summ,N,stdin);
    summ[strlen(summ)-1]='\0';
    int point=0;//показывает какая частьь суммы сейчас считывается, 0 - рубли, 1 - копейки
    int rub=0,kop=0;
    int i =0;
    while(summ[i])
    {
        if(summ[i]!='.' || summ[i]!='\0')
        {
                if (summ[i]<'0' || summ[i]>'9')
                {
                    printf("Incorrect");
                    return 0;
                }
        }
        if (summ[i]>='0' && summ[i]<='9')
        {
            if (!point)
            {
                rub = rub*10+ (int)(summ[i]-'0');
            }
            else
            {
                kop = kop*10 + (int)(summ[i]-'0');
            }
        }
        else if (summ[i]=='.')
        {
            point=1;
        }
        i++;
    }
    int kop1=kop%100;
    while (kop/100!=0)
    {
        rub+=(kop/100);
        kop=kop/100;
    }
    printf("%d rub. %d kop.",rub,kop1);

    return 0;
}

