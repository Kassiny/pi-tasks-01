//Написать программу, подсчитывающую сумму чисел во введенной строке
#include <stdio.h>

int main(void)
{
    char str[256];
    fgets(str,256,stdin);
    int i =0;
    int sum=0;
    int nextItem =0;//число, которое программа будет прибавлять к сумме
    while (str[i])
    {
        if (str[i]<='9' && str[i]>='0')
        {
            nextItem= nextItem*10+(int)(str[i]-'0');


        }
        else
        {
            sum+=nextItem;
            nextItem=0;
        }
    i++;
    }
    printf("%d",sum);
    return 0;
}

